@file:Suppress("IllegalIdentifier")

package com.reviewsviewer.domain.usecases

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import com.reviewsviewer.data.entity.ReviewEntity
import com.reviewsviewer.data.repository.ReviewEntityDataSource
import org.junit.Before
import org.junit.Test
import rx.Observable.just
import rx.Scheduler
import rx.observers.TestSubscriber
import java.util.*
import kotlin.test.assertEquals

class GetReviewsListTest {

    private val mReviewEntityDataSource: ReviewEntityDataSource = mock()
    private val mMockScheduler: Scheduler = mock()

    private lateinit var mGetReviewsList: GetReviewsList
    private lateinit var mTestSubscriber: TestSubscriber<List<com.reviewsviewer.domain.Review>>

    @Before fun setUp() {
        mTestSubscriber = TestSubscriber.create()
        mGetReviewsList = GetReviewsList(mMockScheduler, mMockScheduler, mReviewEntityDataSource)
    }

    @Test fun `should get particular review`() {
        val reviews = createReviewsList()

        assumeDataSourceHasRequestedReviews(reviews)

        mGetReviewsList.call().subscribe(mTestSubscriber)

        assertEquals(reviews.size, mTestSubscriber.onNextEvents[0].size)
    }

    private fun assumeDataSourceHasRequestedReviews(reviews: ArrayList<ReviewEntity>) {
        whenever(mReviewEntityDataSource.reviews()).thenReturn(just<List<ReviewEntity>>(reviews))
    }

    private fun createReviewsList() = ArrayList<ReviewEntity>().apply {
        add(ReviewEntity())
    }
}


