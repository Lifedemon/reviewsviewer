@file:Suppress("IllegalIdentifier")

package com.reviewsviewer.domain.usecases

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import com.reviewsviewer.data.entity.ReviewEntity
import com.reviewsviewer.data.repository.ReviewEntityDataSource
import org.junit.Before
import org.junit.Test
import org.mockito.Matchers.anyInt
import rx.Observable
import rx.Scheduler
import rx.observers.TestSubscriber
import kotlin.test.assertEquals

class GetReviewDetailsTest {

    private val mReviewEntityDataSource: ReviewEntityDataSource = mock()
    private val mScheduler: Scheduler = mock()

    private lateinit var mGetReviewDetails: GetReviewDetails
    private lateinit var mTestSubscriber: TestSubscriber<com.reviewsviewer.domain.Review>


    companion object {
        private val FAKE_REVIEW_ID = 123
    }

    @Before fun setUp() {
        mTestSubscriber = TestSubscriber.create()
        mGetReviewDetails = GetReviewDetails(mScheduler, mScheduler, mReviewEntityDataSource)
    }

    @Test fun `should get particular review`() {
        val reviewEntity = createReviewEntity()
        assumeDataSourceHasRequestedReview(reviewEntity)

        mGetReviewDetails.setReviewId(FAKE_REVIEW_ID)
        mGetReviewDetails.call().subscribe(mTestSubscriber)

        assertEquals(FAKE_REVIEW_ID, mTestSubscriber.onNextEvents[0].id)
    }

    private fun createReviewEntity() = ReviewEntity().apply {
        id = FAKE_REVIEW_ID
    }

    private fun assumeDataSourceHasRequestedReview(reviewEntity: ReviewEntity) {
        whenever(mReviewEntityDataSource.reviews(anyInt())).thenReturn(
                Observable.just(reviewEntity))
    }
}
