@file:Suppress("IllegalIdentifier")

package com.reviewsviewer.domain.usecases

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import com.reviewsviewer.data.entity.ReviewEntity
import com.reviewsviewer.data.repository.ReviewEntityDataSource
import org.junit.Before
import org.junit.Test
import rx.Observable.just
import rx.Scheduler
import rx.observers.TestSubscriber
import java.util.*
import kotlin.test.assertEquals

class SearchByTitleAndMessageTest {

    private val mMockReviewEntityDataSource: ReviewEntityDataSource = mock()
    private val mMockScheduler: Scheduler = mock()

    private lateinit var mSearchByTitleAndMessage: SearchByTitleAndMessage
    private lateinit var mTestSubscriber: TestSubscriber<List<com.reviewsviewer.domain.Review>>

    companion object {
        private val FAKE_REVIEW_TITLE = "Fake Title"
    }

    @Before fun setUp() {
        mTestSubscriber = TestSubscriber.create()
        mSearchByTitleAndMessage = SearchByTitleAndMessage(mMockScheduler, mMockScheduler, mMockReviewEntityDataSource)
    }

    @Test fun `should search by title`() {
        val reviews = createReviewsList()

        assumeDataSourceHasSearchedContent(reviews)

        mSearchByTitleAndMessage.setSearchedTitle(FAKE_REVIEW_TITLE)
        mSearchByTitleAndMessage.call().subscribe(mTestSubscriber)

        assertEquals(reviews.size, mTestSubscriber.onNextEvents[0].size)
    }

    private fun assumeDataSourceHasSearchedContent(reviewses: ArrayList<ReviewEntity>) {
        whenever(mMockReviewEntityDataSource.searchReviewsByTitle(FAKE_REVIEW_TITLE)).thenReturn(
                just<List<ReviewEntity>>(reviewses))
    }

    private fun createReviewsList() = ArrayList<ReviewEntity>().apply {
        add(ReviewEntity().apply { title = FAKE_REVIEW_TITLE })
    }
}