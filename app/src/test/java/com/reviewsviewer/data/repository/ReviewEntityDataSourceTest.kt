@file:Suppress("IllegalIdentifier")

package com.reviewsviewer.data.repository

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import com.reviewsviewer.data.entity.ReviewEntity
import com.reviewsviewer.data.repository.datastore.DatabaseReviewEntityStore
import com.reviewsviewer.data.repository.datastore.ServerReviewEntityStore
import com.reviewsviewer.domain.usecases.SimpleSubscriber
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyZeroInteractions
import rx.Observable.just
import java.util.*

class ReviewEntityDataSourceTest {

    private val mDatabaseReviewEntityStore: DatabaseReviewEntityStore = mock()
    private val mServerReviewEntityStore: ServerReviewEntityStore = mock()

    private lateinit var mReviewEntityDataSource: ReviewEntityDataSource

    companion object {
        private val FAKE_REVIEW_ID = 31
    }

    @Before fun setUp() {
        mReviewEntityDataSource = ReviewEntityDataSource(mDatabaseReviewEntityStore, mServerReviewEntityStore)
    }

    @Test fun `should query database on getting reviews`() {
        val reviewsList = createReviewsList()

        assumeDatabaseIsEmpty()
        assumeServerHasRequestedContent(reviewsList)

        mReviewEntityDataSource.reviews().subscribe(SimpleSubscriber<List<ReviewEntity>>())

        verify(mDatabaseReviewEntityStore).queryForAll()
    }

    @Test fun `should query server on getting reviews if database does not have them`() {
        val reviewsList = createReviewsList()

        assumeDatabaseIsEmpty()
        assumeServerHasRequestedContent(reviewsList)

        mReviewEntityDataSource.reviews().subscribe(SimpleSubscriber<List<ReviewEntity>>())

        verify(mServerReviewEntityStore).reviewEntityList()
    }

    @Test fun `should save retrieved reviews from server on getting reviews`() {
        val reviewsList = createReviewsList()

        assumeDatabaseIsEmpty()
        assumeServerHasRequestedContent(reviewsList)

        mReviewEntityDataSource.reviews().subscribe(SimpleSubscriber<List<ReviewEntity>>())

        verify(mDatabaseReviewEntityStore).saveAll(reviewsList)
    }

    private fun createReviewsList() = ArrayList<ReviewEntity>().apply {
        add(ReviewEntity())
    }

    private fun assumeDatabaseIsEmpty() {
        whenever(mDatabaseReviewEntityStore.queryForAll()).thenReturn(
                just<List<ReviewEntity>>(ArrayList<ReviewEntity>()))
    }

    private fun assumeServerHasRequestedContent(reviewList: List<ReviewEntity>) {
        whenever(mServerReviewEntityStore.reviewEntityList()).thenReturn(just<List<ReviewEntity>>(reviewList))
    }

    @Test fun `should not query server on getting reviews if database has them`() {
        val reviewEntities = createReviewsList()
        assumeDatabaseHasRequestsContent(reviewEntities)
        assumeServerHasRequestedContent(reviewEntities)

        mReviewEntityDataSource.reviews().subscribe()

        verify(mDatabaseReviewEntityStore).queryForAll()
        verifyZeroInteractions(mServerReviewEntityStore)
    }

    private fun assumeDatabaseHasRequestsContent(reviewEntities: List<ReviewEntity>) {
        whenever(mDatabaseReviewEntityStore.queryForAll()).thenReturn(just<List<ReviewEntity>>(reviewEntities))
    }

    @Test fun `should query only database on getting particular review`() {
        val reviewEntity = ReviewEntity()
        whenever(mDatabaseReviewEntityStore.queryForId(FAKE_REVIEW_ID)).thenReturn(
                just(reviewEntity))

        mReviewEntityDataSource.reviews(FAKE_REVIEW_ID).subscribe()

        verify(mDatabaseReviewEntityStore).queryForId(FAKE_REVIEW_ID)
    }
}
