package com.reviewsviewer.presentation.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import com.reviewsviewer.R;
import com.reviewsviewer.presentation.model.ReviewModel;
import com.reviewsviewer.presentation.presenter.ReviewDetailsPresenter;
import com.reviewsviewer.presentation.view.ReviewDetailsView;
import com.squareup.picasso.Picasso;
import javax.inject.Inject;

/**
 * Activity that shows details of a certain reviews.
 * Here it was intended to submit review, but due to lack of time, this View will show just details
 * of each review.
 */
public class ReviewDetailsActivity extends DiAppCompatActivity implements ReviewDetailsView {

  private static final String INTENT_EXTRA_PARAM_REVIEW_ID = "INTENT_PARAM_REVIEW_ID";
  private static final String INSTANCE_STATE_PARAM_REVIEW_ID = "STATE_PARAM_REVIEW_ID";

  @Inject ReviewDetailsPresenter mReviewDetailsPresenter;
  @Inject Picasso mPicasso;

  //Content Views
  @BindView(R.id.title) TextView mTextViewTitle;
  @BindView(R.id.message) TextView mTextViewMessage;
  @BindView(R.id.author) TextView mTextViewAuthor;
  @BindView(R.id.date) TextView mTextViewDate;
  @BindView(R.id.rating) RatingBar mRatingBar;

  //Data Loading Views
  @BindView(R.id.progress_layout) RelativeLayout mProgressView;
  @BindView(R.id.retry_layout) RelativeLayout mRetryView;
  @BindView(R.id.retry_button) Button mRetryButton;

  private int mReviewId;

  public static Intent getCallingIntent(Context context, int reviewId) {
    Intent callingIntent = new Intent(context, ReviewDetailsActivity.class);
    callingIntent.putExtra(INTENT_EXTRA_PARAM_REVIEW_ID, reviewId);

    return callingIntent;
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_review_details);

    initialize(savedInstanceState);
    setUpUI();
  }

  @Override protected void onSaveInstanceState(Bundle outState) {
    if (outState != null) {
      outState.putInt(INSTANCE_STATE_PARAM_REVIEW_ID, mReviewId);
    }
    super.onSaveInstanceState(outState);
  }

  /**
   * Initializes this activity.
   */
  private void initialize(Bundle savedInstanceState) {
    if (savedInstanceState == null) {
      this.mReviewId = getIntent().getIntExtra(INTENT_EXTRA_PARAM_REVIEW_ID, -1);
    } else {
      this.mReviewId = savedInstanceState.getInt(INSTANCE_STATE_PARAM_REVIEW_ID);
    }
    mReviewDetailsPresenter.setView(this);
    mReviewDetailsPresenter.initialize(mReviewId);
  }

  private void setUpUI() {
    //        setSupportActionBar(mToolbar);

    mRetryButton.setOnClickListener(v -> onButtonRetryClick());
  }

  @Override public void onResume() {
    super.onResume();
    this.mReviewDetailsPresenter.resume();
  }

  @Override public void onPause() {
    super.onPause();
    mReviewDetailsPresenter.pause();
  }

  @Override public void onDestroy() {
    super.onDestroy();
    mReviewDetailsPresenter.destroy();
  }

  @Override public void showLoading() {
    mProgressView.setVisibility(View.VISIBLE);
    setProgressBarIndeterminateVisibility(true);
  }

  @Override public void hideLoading() {
    mProgressView.setVisibility(View.GONE);
    setProgressBarIndeterminateVisibility(false);
  }

  @Override public void showRetry() {
    mRetryView.setVisibility(View.VISIBLE);
  }

  @Override public void hideRetry() {
    mRetryView.setVisibility(View.GONE);
  }

  @Override public void showError(String message) {
    showToastMessage(message);
  }

  private void showToastMessage(String message) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
  }

  /**
   * Loads all reviews.
   */
  private void loadReviewDetails() {
    if (mReviewDetailsPresenter != null) {
      mReviewDetailsPresenter.initialize(mReviewId);
    }
  }

  void onButtonRetryClick() {
    loadReviewDetails();
  }

  @Override public void renderReview(ReviewModel review) {
    if (review != null) {
      mTextViewTitle.setText(review.getTitle());
      mTextViewAuthor.setText(review.getAuthor());
      mTextViewDate.setText(review.getDate());
      mTextViewMessage.setText(review.getMessage());
      mRatingBar.setRating(review.getRating());
    }
  }
}
