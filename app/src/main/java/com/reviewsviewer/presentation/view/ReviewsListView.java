/**
 * Copyright (C) 2014 android10.org. All rights reserved.
 *
 * @author Fernando Cejas (the android10 coder)
 */
package com.reviewsviewer.presentation.view;

import com.reviewsviewer.presentation.model.ReviewModel;

import java.util.Collection;

/**
 * Interface representing a View in a model view presenter (MVP) pattern.
 * In this case is used as a view representing a list of {@link ReviewModel}.
 */
public interface ReviewsListView extends LoadDataView {
  /**
   * Render a reviews list in the UI.
   *
   * @param reviewModelCollection The collection of {@link ReviewModel} that will be shown.
   */
  void renderReviewsList(Collection<ReviewModel> reviewModelCollection);

  /**
   * View a {@link ReviewModel} profile/details.
   *
   * @param reviewModel The reviews that will be shown.
   */
  void viewReview(ReviewModel reviewModel);

  /**
   * Highlights text entries in list items.
   *
   * @param textToHighlight text to highlight.
   */
  void highlightTextInList(String textToHighlight);
}
