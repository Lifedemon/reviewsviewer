package com.reviewsviewer.presentation.view;

import com.reviewsviewer.presentation.model.ReviewModel;

/**
 * Interface representing a View in a model view presenter (MVP) pattern.
 * In this case is used as a view representing a reviews profile.
 */
public interface ReviewDetailsView extends LoadDataView {
  /**
   * Render a reviews in the UI.
   *
   * @param review The {@link ReviewModel} that will be shown.
   */
  void renderReview(ReviewModel review);
}
