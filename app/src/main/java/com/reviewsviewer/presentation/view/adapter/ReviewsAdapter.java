package com.reviewsviewer.presentation.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.reviewsviewer.R;
import com.reviewsviewer.presentation.model.ReviewModel;
import com.reviewsviewer.presentation.view.utils.ImageRoundedCornersTransformation;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import java.util.Collection;
import java.util.List;

/**
 * Adapter that manages a collection of {@link ReviewModel}.
 */
public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ReviewViewHolder> {

  private final LayoutInflater mLayoutInflater;
  private final Context mContext;
  private final Picasso mPicasso;
  private List<ReviewModel> mReviewModelsList;
  private OnItemClickListener mOnItemClickListener;
  private Transformation mImageTransformation;

  //Highlighting
  private String mTextToHighlight;
  private static final String STRING_PREPARED_HIGHLIGHT_MARKUP = "<font color='red'>%s</font>";

  public ReviewsAdapter(Context context, Collection<ReviewModel> reviewModelsCollection,
      Picasso picasso) {
    validateReviewsCollection(reviewModelsCollection);
    mContext = context;
    mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    mReviewModelsList = (List<ReviewModel>) reviewModelsCollection;
    mPicasso = picasso;

    setupTransformation();
  }

  public void highlightText(String textToHighlight) {
    mTextToHighlight = textToHighlight;
  }

  @Override public int getItemCount() {
    return (mReviewModelsList != null) ? mReviewModelsList.size() : 0;
  }

  @Override public ReviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = mLayoutInflater.inflate(R.layout.list_item_review, parent, false);
    ReviewViewHolder reviewViewHolder = new ReviewViewHolder(view);

    return reviewViewHolder;
  }

  @Override public void onBindViewHolder(ReviewViewHolder holder, final int position) {
    final ReviewModel reviewModel = mReviewModelsList.get(position);
    renderReview(holder, reviewModel);

    holder.itemView.setOnClickListener(v -> {
      if (ReviewsAdapter.this.mOnItemClickListener != null) {
        ReviewsAdapter.this.mOnItemClickListener.onItemClicked(reviewModel);
      }
    });
  }

  private void renderReview(ReviewViewHolder holder, ReviewModel reviewModel) {
    showHighlighted(holder.mTextViewTitle, reviewModel.getTitle());
    showHighlighted(holder.mTextViewMessage, reviewModel.getMessage());
    holder.mTextViewAuthor.setText(reviewModel.getAuthor());
    holder.mTextViewDate.setText(reviewModel.getDate());
    showRating(holder, reviewModel.getRating());
    showCountryFlag(holder, reviewModel);
  }

  private void showRating(ReviewViewHolder holder, Float rating) {
    holder.mRatingBar.setRating(rating);
  }

  private void showCountryFlag(ReviewViewHolder holder, ReviewModel reviewModel) {
    mPicasso.load(reviewModel.getLanguageCode())
        .placeholder(R.drawable.ic_crop_original_black)
        .error(R.drawable.ic_error_outline_black)
        .transform(mImageTransformation);
    //        .into(holder.mCountryFlagImageView);
  }

  private void showHighlighted(TextView textView, String original) {
    if (mTextToHighlight != null && !mTextToHighlight.isEmpty()) {
      String preparedMarkup = String.format(STRING_PREPARED_HIGHLIGHT_MARKUP, mTextToHighlight);
      String highlighted = original.replaceAll(mTextToHighlight, preparedMarkup);
      textView.setText(Html.fromHtml(highlighted));
    } else {
      textView.setText(original);
    }
  }

  @Override public long getItemId(int position) {
    return position;
  }

  public void setReviews(Collection<ReviewModel> reviewModelsCollection) {
    validateReviewsCollection(reviewModelsCollection);
    mReviewModelsList = (List<ReviewModel>) reviewModelsCollection;
    notifyDataSetChanged();
  }

  public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
    mOnItemClickListener = onItemClickListener;
  }

  private void setupTransformation() {
    int radius =
        (int) mContext.getResources().getDimension(R.dimen.list_view_row_icon_rounding_radius);
    mImageTransformation = new ImageRoundedCornersTransformation(radius, 0);
  }

  private void validateReviewsCollection(Collection<ReviewModel> reviewModelsCollection) {
    if (reviewModelsCollection == null) {
      throw new IllegalArgumentException("The list cannot be null");
    }
  }

  public interface OnItemClickListener {
    void onItemClicked(ReviewModel reviewModel);
  }

  static class ReviewViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.title) TextView mTextViewTitle;
    @BindView(R.id.message) TextView mTextViewMessage;
    @BindView(R.id.author) TextView mTextViewAuthor;
    @BindView(R.id.date) TextView mTextViewDate;
    @BindView(R.id.rating) RatingBar mRatingBar;
    //    @BindView(R.id.flag) ImageView mCountryFlagImageView;

    public ReviewViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
