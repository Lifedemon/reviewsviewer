package com.reviewsviewer.presentation.view.activity;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.Toast;
import butterknife.BindView;
import com.reviewsviewer.R;
import com.reviewsviewer.presentation.model.ReviewModel;
import com.reviewsviewer.presentation.navigation.Navigator;
import com.reviewsviewer.presentation.presenter.ReviewListPresenter;
import com.reviewsviewer.presentation.view.ReviewsListView;
import com.reviewsviewer.presentation.view.adapter.ReviewsAdapter;
import com.reviewsviewer.presentation.view.utils.ImageRoundedCornersTransformation;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import java.util.ArrayList;
import java.util.Collection;
import javax.inject.Inject;

public class ReviewsListActivity extends DiAppCompatActivity implements ReviewsListView {

  @Inject ReviewListPresenter mReviewListPresenter;
  @Inject Navigator mNavigator;
  @Inject Picasso mPicasso;

  //Toolbar Views
  @BindView(R.id.toolbar) Toolbar mToolbar;

  //Content Views
  @BindView(R.id.reviews_list) RecyclerView mReviewsListView;

  //Data Loading Views
  @BindView(R.id.progress_layout) RelativeLayout mProgressView;
  @BindView(R.id.retry_layout) RelativeLayout mRetryView;
  @BindView(R.id.retry_button) Button mRetryButton;

  private CheckBox mSortMenuCheckBox;

  private ReviewsAdapter mReviewsListAdapter;
  private Transformation mImageTransformation;

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_review_list);

    initialize();
    loadReviewList();
    setUpUI();
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.reviews_list, menu);
    setUpSearchMenu(menu);
    setUpSortMenu(menu);
    return true;
  }

  private void setUpSortMenu(Menu menu) {
    mSortMenuCheckBox = (CheckBox) menu.
        findItem(R.id.menu_sort).
        getActionView().
        findViewById(R.id.sort_toggle_button);

    mSortMenuCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
      mReviewListPresenter.sort(!isChecked);
    });
  }

  private void setUpSearchMenu(Menu menu) {
    // Associate searchable configuration with the SearchView
    SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
    SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
    searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

    searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
      @Override public boolean onQueryTextSubmit(String query) {
        return false;
      }

      @Override public boolean onQueryTextChange(String newText) {
        mReviewListPresenter.searchByTitle(newText);
        return true;
      }
    });
    searchView.setOnCloseListener(() -> {
      mReviewListPresenter.searchByTitle(null);
      return false;
    });
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();

    if (id == R.id.menu_sort) {
      onSortMenuSelected();
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  private void onSortMenuSelected() {
    mSortMenuCheckBox.toggle();
  }

  @Override public void onResume() {
    super.onResume();
    mReviewListPresenter.resume();
  }

  @Override public void onPause() {
    super.onPause();
    mReviewListPresenter.pause();
  }

  @Override public void onDestroy() {
    super.onDestroy();
    mReviewListPresenter.destroy();
  }

  private void initialize() {
    mReviewListPresenter.setView(this);
  }

  private void setUpUI() {
    setSupportActionBar(mToolbar);
    getSupportActionBar().setTitle("");

    mReviewsListView.setLayoutManager(new LinearLayoutManager(this));

    mReviewsListAdapter = new ReviewsAdapter(this, new ArrayList<>(), mPicasso);
    mReviewsListAdapter.setOnItemClickListener(onItemClickListener);
    mReviewsListView.setAdapter(mReviewsListAdapter);

    mRetryButton.setOnClickListener(v -> onButtonRetryClick());

    setupTransformation();
  }

  private void setupTransformation() {
    int radius = (int) getResources().getDimension(R.dimen.list_view_row_icon_rounding_radius);
    mImageTransformation = new ImageRoundedCornersTransformation(radius, 0);
  }

  @Override public void showLoading() {
    mProgressView.setVisibility(View.VISIBLE);
    setProgressBarIndeterminateVisibility(true);
  }

  @Override public void hideLoading() {
    mProgressView.setVisibility(View.GONE);
    setProgressBarIndeterminateVisibility(false);
  }

  @Override public void showRetry() {
    mRetryView.setVisibility(View.VISIBLE);
  }

  @Override public void hideRetry() {
    mRetryView.setVisibility(View.GONE);
  }

  @Override public void showError(String message) {
    showToastMessage(message);
  }

  protected void showToastMessage(String message) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
  }

  /**
   * Loads all reviews.
   */
  private void loadReviewList() {
    mReviewListPresenter.initialize();
  }

  void onButtonRetryClick() {
    loadReviewList();
  }

  private ReviewsAdapter.OnItemClickListener onItemClickListener = reviewModel -> {
    if (ReviewsListActivity.this.mReviewListPresenter != null && reviewModel != null) {
      ReviewsListActivity.this.mReviewListPresenter.onReviewClicked(reviewModel);
    }
  };

  @Override public void renderReviewsList(Collection<ReviewModel> reviewsModels) {
    if (reviewsModels != null) {
      this.mReviewsListAdapter.setReviews(reviewsModels);
    }
  }

  @Override public void viewReview(ReviewModel reviewModel) {
    mNavigator.navigateToReviewDetails(this, reviewModel.getId());
  }

  @Override public void highlightTextInList(String textToHighlight) {
    mReviewsListAdapter.highlightText(textToHighlight);
  }
}
