package com.reviewsviewer.presentation;

import android.app.Application;
import com.reviewsviewer.presentation.di.modules.AppModule;
import dagger.ObjectGraph;

public class ReviewsViewerApplication extends Application {

  @Override public void onCreate() {
    super.onCreate();
    sObjectGraph = ObjectGraph.create(new AppModule(this));
  }

  private static ObjectGraph sObjectGraph;

  public static ObjectGraph getScopedGraph(Object... modules) {
    return sObjectGraph.plus(modules);
  }
}
