package com.reviewsviewer.presentation.presenter;

import android.support.annotation.NonNull;

import com.reviewsviewer.domain.Review;
import com.reviewsviewer.domain.usecases.GetReviewsList;
import com.reviewsviewer.domain.usecases.SearchByTitleAndMessage;
import com.reviewsviewer.domain.usecases.SimpleSubscriber;
import com.reviewsviewer.presentation.mapper.review.ReviewToReviewModel;
import com.reviewsviewer.presentation.model.ReviewModel;
import com.reviewsviewer.presentation.view.ReviewsListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.inject.Inject;

/**
 * {@link Presenter} that controls communication between views and models of the presentation
 * layer.
 */
public class ReviewListPresenter extends SimplePresenter {

  private ReviewsListView mViewList;

  //Use cases
  private final GetReviewsList mGetReviewsListUseCase;
  private final SearchByTitleAndMessage mSearchByTitleAndMessageUseCase;

  //Transformers
  private ReviewToReviewModel mReviewModelTransformer;

  private ReviewModelRatingComparator mModelRatingComparator =
      new ReviewModelRatingComparator(true);
  private String mSearchedTitle;

  @Inject public ReviewListPresenter(GetReviewsList getReviewsListUseCase,
      SearchByTitleAndMessage searchByTitleAndMessageUseCase) {

    mGetReviewsListUseCase = getReviewsListUseCase;
    mSearchByTitleAndMessageUseCase = searchByTitleAndMessageUseCase;

    createTransformers();
  }

  private void createTransformers() {
    mReviewModelTransformer = new ReviewToReviewModel();
  }

  public void setView(@NonNull ReviewsListView view) {
    mViewList = view;
  }

  @Override public void destroy() {
    mGetReviewsListUseCase.unsubscribe();
  }

  /**
   * Initializes the presenter by start retrieving the reviews list.
   */
  public void initialize() {
    loadReviewsList();
  }

  public void sort(boolean ascending) {
    mModelRatingComparator = new ReviewModelRatingComparator(ascending);

    if (mSearchedTitle == null || mSearchedTitle.isEmpty()) {
      loadReviewsList();
      mViewList.highlightTextInList(null);
    } else {
      searchByTitle(mSearchedTitle);
    }
  }

  public void searchByTitle(String title) {
    mSearchedTitle = title;

    if (title == null || title.isEmpty()) {
      loadReviewsList();
    } else {
      showViewLoading();
      mSearchByTitleAndMessageUseCase.setSearchedTitle(title);
      mSearchByTitleAndMessageUseCase.call(new ReviewsListSubscriber());
    }

    mViewList.highlightTextInList(title);
  }

  public void onReviewClicked(ReviewModel reviewModel) {
    mViewList.viewReview(reviewModel);
  }

  /**
   * Loads all reviews.
   */
  public void loadReviewsList() {
    hideViewRetry();
    showViewLoading();
    getReviewsList();
  }

  private void showViewLoading() {
    mViewList.showLoading();
  }

  private void hideViewLoading() {
    mViewList.hideLoading();
  }

  private void showViewRetry() {
    mViewList.showRetry();
  }

  private void hideViewRetry() {
    mViewList.hideRetry();
  }

  private void showErrorMessage() {
    //TODO implement
    String errorMessage = "Error happened";
    mViewList.showError(errorMessage);
  }

  private void showReviewsListInView(List<Review> reviewList) {
    final List<ReviewModel> reviewModelsList =
        new ArrayList<>(mReviewModelTransformer.transform(reviewList));

    Collections.sort(reviewModelsList, mModelRatingComparator);

    mViewList.renderReviewsList(reviewModelsList);
  }

  private void getReviewsList() {
    mGetReviewsListUseCase.call(new ReviewsListSubscriber());
  }

  private final class ReviewsListSubscriber extends SimpleSubscriber<List<Review>> {

    @Override public void onCompleted() {
      ReviewListPresenter.this.hideViewLoading();
    }

    @Override public void onError(Throwable e) {
      ReviewListPresenter.this.hideViewLoading();
      ReviewListPresenter.this.showErrorMessage();
      ReviewListPresenter.this.showViewRetry();
    }

    @Override public void onNext(List<Review> reviews) {
      ReviewListPresenter.this.showReviewsListInView(reviews);
    }
  }

  private class ReviewModelRatingComparator implements Comparator<ReviewModel> {
    private boolean mIsAscending;

    ReviewModelRatingComparator(boolean isAscending) {
      mIsAscending = isAscending;
    }

    @Override public int compare(@NonNull ReviewModel lhs, @NonNull ReviewModel rhs) {
      return lhs.getRating().compareTo(rhs.getRating()) * (mIsAscending ? 1 : -1);
    }
  }
}

