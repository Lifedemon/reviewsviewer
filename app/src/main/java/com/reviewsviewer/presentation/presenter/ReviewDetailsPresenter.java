package com.reviewsviewer.presentation.presenter;

import android.support.annotation.NonNull;

import com.reviewsviewer.domain.Review;
import com.reviewsviewer.domain.usecases.GetReviewDetails;
import com.reviewsviewer.domain.usecases.SimpleSubscriber;
import com.reviewsviewer.presentation.mapper.review.ReviewToReviewModel;
import com.reviewsviewer.presentation.model.ReviewModel;
import com.reviewsviewer.presentation.view.ReviewDetailsView;
import javax.inject.Inject;

/**
 * {@link Presenter} that controls communication between views and models of the presentation
 * layer.
 */
public class ReviewDetailsPresenter extends SimplePresenter {

  private final GetReviewDetails mGetReviewDetailsUseCase;
  private final ReviewToReviewModel mReviewModelTransformer;
  /**
   * id used to retrieve reviews details
   */
  private int mReviewId;
  private ReviewDetailsView mViewDetailsView;

  @Inject public ReviewDetailsPresenter(GetReviewDetails getReviewDetailsUseCase) {
    mGetReviewDetailsUseCase = getReviewDetailsUseCase;
    mReviewModelTransformer = new ReviewToReviewModel();
  }

  public void setView(@NonNull ReviewDetailsView view) {
    mViewDetailsView = view;
  }

  @Override public void destroy() {
    mGetReviewDetailsUseCase.unsubscribe();
  }

  /**
   * Initializes the presenter by start retrieving reviews details.
   */
  public void initialize(int reviewId) {
    mReviewId = reviewId;
    loadReviewDetails();
  }

  /**
   * Loads reviews details.
   */
  private void loadReviewDetails() {
    hideViewRetry();
    showViewLoading();
    getReviewDetails();
  }

  private void showViewLoading() {
    mViewDetailsView.showLoading();
  }

  private void hideViewLoading() {
    mViewDetailsView.hideLoading();
  }

  private void showViewRetry() {
    mViewDetailsView.showRetry();
  }

  private void hideViewRetry() {
    mViewDetailsView.hideRetry();
  }

  private void showErrorMessage() {
    //TODO implement
    String errorMessage = "Error happened";
    mViewDetailsView.showError(errorMessage);
  }

  private void showReviewDetailsInView(Review review) {
    final ReviewModel reviewModel = mReviewModelTransformer.transform(review);
    mViewDetailsView.renderReview(reviewModel);
  }

  private void getReviewDetails() {
    mGetReviewDetailsUseCase.setReviewId(mReviewId);
    mGetReviewDetailsUseCase.call(new ReviewDetailsSubscriber());
  }

  private final class ReviewDetailsSubscriber extends SimpleSubscriber<Review> {

    @Override public void onCompleted() {
      ReviewDetailsPresenter.this.hideViewLoading();
    }

    @Override public void onError(Throwable e) {
      ReviewDetailsPresenter.this.hideViewLoading();
      ReviewDetailsPresenter.this.showErrorMessage();
      ReviewDetailsPresenter.this.showViewRetry();
    }

    @Override public void onNext(Review review) {
      ReviewDetailsPresenter.this.showReviewDetailsInView(review);
    }
  }
}
