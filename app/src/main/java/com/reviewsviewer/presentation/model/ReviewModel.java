package com.reviewsviewer.presentation.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Data @Accessors(prefix = "m") public class ReviewModel {

  private Integer mId;

  private String mTitle;

  private Float mRating;

  private String mAuthor;

  private String mMessage;

  private String mDate;

  private String mLanguageCode;
}
