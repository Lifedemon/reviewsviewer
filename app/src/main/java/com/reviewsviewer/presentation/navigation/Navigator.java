package com.reviewsviewer.presentation.navigation;

import android.content.Context;
import android.content.Intent;

import com.reviewsviewer.presentation.view.activity.ReviewDetailsActivity;
import javax.inject.Inject;

/**
 * Class used to navigate through the application.
 */
public class Navigator {

  @Inject public Navigator() {
  }

  /**
   * Goes to the reviews details screen.
   *
   * @param context A Context needed to open the activity.
   */
  public void navigateToReviewDetails(Context context, int reviewId) {
    if (context != null) {
      Intent intentToLaunch = ReviewDetailsActivity.getCallingIntent(context, reviewId);
      context.startActivity(intentToLaunch);
    }
  }
}
