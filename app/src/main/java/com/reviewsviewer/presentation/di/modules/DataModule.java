package com.reviewsviewer.presentation.di.modules;

import com.reviewsviewer.data.repository.ReviewEntityDataSource;
import com.reviewsviewer.domain.repository.ReviewRepository;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

@Module(library = true, complete = false) public class DataModule {

  @Provides @Singleton
  public ReviewRepository providesReviewRepository(ReviewEntityDataSource repository) {
    return repository;
  }
}
