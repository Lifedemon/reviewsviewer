package com.reviewsviewer.presentation.di.modules;

import com.reviewsviewer.presentation.view.activity.DiAppCompatActivity;
import com.reviewsviewer.presentation.view.activity.ReviewDetailsActivity;
import com.reviewsviewer.presentation.view.activity.ReviewsListActivity;
import dagger.Module;

@Module(injects = {
    DiAppCompatActivity.class, ReviewsListActivity.class, ReviewDetailsActivity.class
}, addsTo = AppModule.class) public class DiActivityModule {
}
