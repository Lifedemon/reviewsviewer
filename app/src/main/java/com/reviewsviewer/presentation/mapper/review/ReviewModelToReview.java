package com.reviewsviewer.presentation.mapper.review;

import com.reviewsviewer.domain.Review;
import com.reviewsviewer.domain.mapper.BaseLayerDataTransformer;
import com.reviewsviewer.presentation.model.ReviewModel;

public class ReviewModelToReview extends BaseLayerDataTransformer<ReviewModel, Review> {
  @Override public Review transform(ReviewModel from) {
    Review transformed = new Review();

    transformed.setId(from.getId());
    transformed.setTitle(from.getTitle());
    transformed.setMessage(from.getMessage());
    transformed.setAuthor(from.getAuthor());
    transformed.setDate(from.getDate());
    transformed.setRating(from.getRating());
    transformed.setLanguageCode(from.getLanguageCode());

    return transformed;
  }
}
