package com.reviewsviewer.presentation.mapper.review;

import com.reviewsviewer.domain.Review;
import com.reviewsviewer.presentation.model.ReviewModel;
import com.reviewsviewer.domain.mapper.BaseLayerDataTransformer;

public class ReviewToReviewModel extends BaseLayerDataTransformer<Review, ReviewModel> {
  @Override public ReviewModel transform(Review from) {
    ReviewModel transformed = new ReviewModel();

    transformed.setId(from.getId());
    transformed.setTitle(from.getTitle());
    transformed.setRating(from.getRating());
    transformed.setAuthor(from.getAuthor());
    transformed.setMessage(from.getMessage());
    transformed.setDate(from.getDate());
    transformed.setLanguageCode(from.getLanguageCode());

    return transformed;
  }
}