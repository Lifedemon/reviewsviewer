package com.reviewsviewer.data.entity;

import com.google.gson.annotations.SerializedName;
import java.util.List;
import lombok.Data;
import lombok.experimental.Accessors;

@Accessors(prefix = "m") @Data public class ReviewsList {

  interface Fields {
    String STATUS = "status";
    String TOTAL_REVIEWS = "total_reviews";
    String DATA = "data";
  }

  @SerializedName(Fields.STATUS)
  private Boolean mStatus;

  @SerializedName(Fields.TOTAL_REVIEWS)
  private Integer mTotalReviews;

  @SerializedName(Fields.DATA)
  private List<ReviewEntity> mData;
}
