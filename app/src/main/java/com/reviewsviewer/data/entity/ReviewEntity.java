package com.reviewsviewer.data.entity;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(prefix = "m")
@DatabaseTable(tableName = "reviews")
public class ReviewEntity {

    public interface Fields {
        String ID = "review_id";
        String RATING = "rating";
        String TITLE = "title";
        String AUTHOR = "author";
        String MESSAGE = "message";
        String DATE = "date";
        String LANGUAGE_CODE = "languageCode";
    }

    @SerializedName(Fields.ID)
    @DatabaseField(id = true, columnName = Fields.ID)
    private Integer mId;

    @SerializedName(Fields.RATING)
    @DatabaseField(columnName = Fields.RATING)
    private Float mRating;

    @SerializedName(Fields.TITLE)
    @DatabaseField(columnName = Fields.TITLE)
    private String mTitle;

    @SerializedName(Fields.AUTHOR)
    @DatabaseField(columnName = Fields.AUTHOR)
    private String mAuthor;

    @SerializedName(Fields.MESSAGE)
    @DatabaseField(columnName = Fields.MESSAGE)
    private String mMessage;

    @SerializedName(Fields.DATE)
    @DatabaseField(columnName = Fields.DATE)
    private String mDate;

    @SerializedName(Fields.LANGUAGE_CODE)
    @DatabaseField(columnName = Fields.LANGUAGE_CODE)
    private String mLanguageCode;
}