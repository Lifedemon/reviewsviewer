package com.reviewsviewer.data.db;

import android.content.Context;
import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.reviewsviewer.data.entity.ReviewEntity;

import java.sql.SQLException;
import javax.inject.Inject;

public class DatabaseManager {

  public static final String LOG_TAG_DB = "Reviews:Database";
  public static final String DEFAULT_DATABASE_NAME = "reviews_viewer";

  private final ConnectionSource mConnectionSource;

  //DAOs
  private Dao<ReviewEntity, Integer> mReviewsDao;

  @Inject public DatabaseManager(Context context) {
    this(context, DEFAULT_DATABASE_NAME);
  }

  public DatabaseManager(Context context, String databaseName) {
    DatabaseHelper databaseHelper = new DatabaseHelper(context, databaseName);
    mConnectionSource =
        databaseHelper.getConnectionSource(); // force database creation/upgrade eagerly
  }

  public Dao<ReviewEntity, Integer> getReviewsDao() {
    if (mReviewsDao == null) {
      createReviewEntityDao();
    }

    return mReviewsDao;
  }

  private void createReviewEntityDao() {
    try {
      mReviewsDao = DaoManager.createDao(mConnectionSource, ReviewEntity.class);
      mReviewsDao.setObjectCache(true);
    } catch (SQLException e) {
      Log.wtf(LOG_TAG_DB, "Creation of Dao<" + ReviewEntity.class + "> failed", e);
    }
  }
}
