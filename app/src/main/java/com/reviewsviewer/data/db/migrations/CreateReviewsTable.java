package com.reviewsviewer.data.db.migrations;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.reviewsviewer.data.entity.ReviewEntity;
import com.reviewsviewer.data.db.DatabaseManager;

import java.sql.SQLException;

public class CreateReviewsTable extends Migration {
  @Override public void up(SQLiteDatabase db, ConnectionSource connectionSource) {
    try {
      TableUtils.createTable(connectionSource, ReviewEntity.class);
    } catch (SQLException e) {
      Log.wtf(DatabaseManager.LOG_TAG_DB, "Creation of ReviewsTable failed", e);
    }
  }
}
