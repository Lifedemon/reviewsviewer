package com.reviewsviewer.data.network;

import com.reviewsviewer.data.entity.ReviewEntity;
import com.reviewsviewer.data.entity.ReviewsList;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import rx.Observable;

/**
 * RestApi for retrieving data from the network.
 */
public interface ReviewsRestService {

  /**
   * Retrieves an {@link rx.Observable} which will emit a List of {@link ReviewEntity}.
   */
  @Headers({
      "User-Agent: Android-App",
  })
  @GET("berlin-l17/tempelhof-2-hour-airport-history-tour-berlin-airlift-more-t23776/reviews.json?rating=0&type=&sortBy=date_of_review&direction=DESC")
  Observable<ReviewsList> reviewsEntityList();
}
