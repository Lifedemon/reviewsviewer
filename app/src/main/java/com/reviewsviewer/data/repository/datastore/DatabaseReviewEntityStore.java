package com.reviewsviewer.data.repository.datastore;

import android.util.Log;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;
import com.reviewsviewer.data.db.DatabaseManager;
import com.reviewsviewer.data.entity.ReviewEntity;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import rx.Observable;

import static rx.Observable.error;
import static rx.Observable.just;

public class DatabaseReviewEntityStore {

  private static final String LOG_TAG = DatabaseReviewEntityStore.class.getSimpleName();
  private static final String PERCENT = "%";

  private Dao<ReviewEntity, Integer> mReviewsDao;

  //Search prepared fields, for faster search
  private PreparedQuery<ReviewEntity> mSearchByTitleQuery;
  private SelectArg mSearchByTitleQuerySelectArg;
  private SelectArg mSearchByMessageQuerySelectArg;

  @Inject public DatabaseReviewEntityStore(DatabaseManager databaseManager) {
    mReviewsDao = databaseManager.getReviewsDao();
    prepareSearchByTitleQuery();
  }

  public Observable<List<ReviewEntity>> queryForAll() {
    return Observable.defer(() -> {
      try {
        return just(mReviewsDao.queryForAll());
      } catch (SQLException e) {
        return error(e);
      }
    });
  }

  public Observable<Void> saveAll(final Collection<ReviewEntity> entities) {
    return Observable.defer(() -> {
      try {
        saveAllSynchronous(entities);
        return just(null);
      } catch (SQLException e) {
        return error(e);
      }
    });
  }

  public void saveAllSynchronous(final Collection<ReviewEntity> entities) throws SQLException {
    TransactionManager.callInTransaction(mReviewsDao.getConnectionSource(), () -> {
      for (ReviewEntity reviewEntity : entities) {
        mReviewsDao.createOrUpdate(reviewEntity);
      }
      return null;
    });
  }

  public Observable<ReviewEntity> queryForId(int reviewId) {
    return Observable.defer(() -> {
      try {
        return just(mReviewsDao.queryForId(reviewId));
      } catch (SQLException e) {
        return error(e);
      }
    });
  }

  public Observable<List<ReviewEntity>> queryForTitle(String title) {
    return Observable.defer(() -> {
      try {
        String value = PERCENT + title + PERCENT;
        mSearchByTitleQuerySelectArg.setValue(value);
        mSearchByMessageQuerySelectArg.setValue(value);
        return just(mReviewsDao.query(mSearchByTitleQuery));
      } catch (SQLException e) {
        return error(e);
      }
    });
  }

  private void prepareSearchByTitleQuery() {
    try {
      QueryBuilder<ReviewEntity, Integer> queryBuilder = mReviewsDao.queryBuilder();
      mSearchByTitleQuerySelectArg = new SelectArg();
      mSearchByMessageQuerySelectArg = new SelectArg();
      queryBuilder.where()
          .like(ReviewEntity.Fields.TITLE, mSearchByTitleQuerySelectArg)
          .or()
          .like(ReviewEntity.Fields.MESSAGE, mSearchByMessageQuerySelectArg);
      mSearchByTitleQuery = queryBuilder.prepare();
    } catch (SQLException e) {
      Log.wtf(LOG_TAG, "Preparing of SearchByTitleQuery failed", e);
    }
  }
}
