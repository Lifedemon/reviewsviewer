package com.reviewsviewer.data.repository.datastore;

import com.reviewsviewer.data.entity.ReviewEntity;

import com.reviewsviewer.data.entity.ReviewsList;
import com.reviewsviewer.data.network.ReviewsRestService;
import java.util.List;

import javax.inject.Inject;
import rx.Observable;

public class ServerReviewEntityStore {

  private final ReviewsRestService mService;

  @Inject public ServerReviewEntityStore(ReviewsRestService service) {
    mService = service;
  }

  public Observable<List<ReviewEntity>> reviewEntityList() {
    return mService.reviewsEntityList().map(ReviewsList::getData);
  }
}
