package com.reviewsviewer.data.repository;

import com.reviewsviewer.data.entity.ReviewEntity;
import com.reviewsviewer.data.repository.datastore.DatabaseReviewEntityStore;
import com.reviewsviewer.data.repository.datastore.ServerReviewEntityStore;
import com.reviewsviewer.domain.repository.ReviewRepository;

import java.util.List;

import javax.inject.Inject;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;

public class ReviewEntityDataSource implements ReviewRepository {

  private DatabaseReviewEntityStore mDatabaseReviewEntityStore;
  private ServerReviewEntityStore mServerReviewEntityStore;

  @Inject public ReviewEntityDataSource(DatabaseReviewEntityStore databaseReviewEntityStore,
      ServerReviewEntityStore serverReviewEntityStore) {
    mDatabaseReviewEntityStore = databaseReviewEntityStore;
    mServerReviewEntityStore = serverReviewEntityStore;
  }

  @Override public Observable<List<ReviewEntity>> reviews() {
    return Observable.create(subscriber -> queryDatabaseForAll(subscriber));
  }

  @Override public Observable<List<ReviewEntity>> searchReviewsByTitle(String title) {
    return mDatabaseReviewEntityStore.queryForTitle(title);
  }

  @Override public Observable<ReviewEntity> reviews(int reviewId) {
    return mDatabaseReviewEntityStore.queryForId(reviewId);
  }

  private void queryDatabaseForAll(final Subscriber<? super List<ReviewEntity>> subscriber) {
    mDatabaseReviewEntityStore.queryForAll().subscribe(new Observer<List<ReviewEntity>>() {
      @Override public void onCompleted() {

      }

      @Override public void onError(Throwable e) {
        subscriber.onError(e);
        fetchServerForAll(subscriber);
      }

      @Override public void onNext(List<ReviewEntity> reviewEntities) {
        if (reviewEntities.size() != 0) {
          subscriber.onNext(reviewEntities);
          subscriber.onCompleted();
        } else {
          fetchServerForAll(subscriber);
        }
      }
    });
  }

  private void fetchServerForAll(Subscriber<? super List<ReviewEntity>> subscriber) {
    mServerReviewEntityStore.reviewEntityList().subscribe(new Observer<List<ReviewEntity>>() {
      @Override public void onCompleted() {
        subscriber.onCompleted();
      }

      @Override public void onError(Throwable e) {
        subscriber.onError(e);
      }

      @Override public void onNext(List<ReviewEntity> reviewEntities) {
        subscriber.onNext(reviewEntities);
        mDatabaseReviewEntityStore.
            saveAll(reviewEntities).
            subscribe();
      }
    });
  }
}
