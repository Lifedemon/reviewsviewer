package com.reviewsviewer.domain.usecases;

import com.reviewsviewer.data.repository.ReviewEntityDataSource;
import com.reviewsviewer.domain.Review;
import com.reviewsviewer.domain.mapper.review.ReviewEntityToReview;

import com.reviewsviewer.presentation.di.modules.RxModule;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import lombok.Setter;
import lombok.experimental.Accessors;
import rx.Observable;
import rx.Scheduler;

@Accessors(prefix = "m") public class SearchByTitleAndMessage extends UseCase<List<Review>> {

  @Setter private String mSearchedTitle;
  private ReviewEntityDataSource mReviewEntityDataSource;
  private final ReviewEntityToReview mReviewTransformer;

  @Inject public SearchByTitleAndMessage(@Named(RxModule.COMPUTATION) Scheduler executionScheduler,
      @Named(RxModule.MAIN_THREAD) Scheduler observingScheduler,
      ReviewEntityDataSource reviewEntityDataSource) {
    super(executionScheduler, observingScheduler);
    mReviewEntityDataSource = reviewEntityDataSource;
    mReviewTransformer = new ReviewEntityToReview();
  }

  @Override protected Observable<List<Review>> call() {
    return this.mReviewEntityDataSource.searchReviewsByTitle(mSearchedTitle)
        .map(mReviewTransformer::transform);
  }
}

