package com.reviewsviewer.domain.usecases;

import com.reviewsviewer.data.repository.ReviewEntityDataSource;
import com.reviewsviewer.domain.Review;
import com.reviewsviewer.domain.mapper.review.ReviewEntityToReview;
import com.reviewsviewer.presentation.di.modules.RxModule;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.Setter;
import lombok.experimental.Accessors;
import rx.Observable;
import rx.Scheduler;

@Accessors(prefix = "m") public class GetReviewDetails extends UseCase<Review> {

  @Setter private int mReviewId;
  private final ReviewEntityToReview mReviewTransformer;

  private ReviewEntityDataSource mReviewEntityDataSource;

  @Inject public GetReviewDetails(@Named(RxModule.COMPUTATION) Scheduler executionScheduler,
      @Named(RxModule.MAIN_THREAD) Scheduler observingScheduler,
      ReviewEntityDataSource reviewEntityDataSource) {
    super(executionScheduler, observingScheduler);
    mReviewTransformer = new ReviewEntityToReview();
    mReviewEntityDataSource = reviewEntityDataSource;
  }

  @Override protected Observable<Review> call() {
    return this.mReviewEntityDataSource.reviews(mReviewId).map(mReviewTransformer::transform);
  }
}
