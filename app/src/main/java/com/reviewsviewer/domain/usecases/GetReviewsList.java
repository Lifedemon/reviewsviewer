package com.reviewsviewer.domain.usecases;

import com.reviewsviewer.data.repository.ReviewEntityDataSource;
import com.reviewsviewer.domain.Review;
import com.reviewsviewer.domain.mapper.review.ReviewEntityToReview;
import com.reviewsviewer.presentation.di.modules.RxModule;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;
import rx.Observable;
import rx.Scheduler;

public class GetReviewsList extends UseCase<List<Review>> {

  private ReviewEntityDataSource mReviewEntityDataSource;
  private final ReviewEntityToReview mReviewTransformer;

  @Inject public GetReviewsList(@Named(RxModule.COMPUTATION) Scheduler executionScheduler,
      @Named(RxModule.MAIN_THREAD) Scheduler observingScheduler,
      ReviewEntityDataSource reviewEntityDataSource) {
    super(executionScheduler, observingScheduler);
    mReviewTransformer = new ReviewEntityToReview();
    mReviewEntityDataSource = reviewEntityDataSource;
  }

  @Override protected Observable<List<Review>> call() {
    return mReviewEntityDataSource.reviews().map(mReviewTransformer::transform);
  }
}

