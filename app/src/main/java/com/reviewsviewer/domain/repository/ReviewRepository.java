package com.reviewsviewer.domain.repository;

import com.reviewsviewer.data.entity.ReviewEntity;
import com.reviewsviewer.domain.Review;

import java.util.List;

import rx.Observable;

/**
 * Interface that represents a Repository for getting {@link Review} related data.
 */
public interface ReviewRepository {
  /**
   * Get an {@link rx.Observable} which will emit a List of {@link ReviewEntity}.
   */
  Observable<List<ReviewEntity>> reviews();

  /**
   * Get an {@link rx.Observable} which will emit a List of {@link ReviewEntity},
   * whom titles match searched title.
   *
   * @param title The reviews' Title used to retrieve reviews data.
   */
  Observable<List<ReviewEntity>> searchReviewsByTitle(String title);

  /**
   * Get an {@link rx.Observable} which will emit a {@link ReviewEntity}.
   *
   * @param reviewId The reviews id used to retrieve reviews data.
   */
  Observable<ReviewEntity> reviews(final int reviewId);
}
