package com.reviewsviewer.domain.mapper.review;

import com.reviewsviewer.data.entity.ReviewEntity;
import com.reviewsviewer.domain.Review;
import com.reviewsviewer.domain.mapper.BaseLayerDataTransformer;

public class ReviewEntityToReview extends BaseLayerDataTransformer<ReviewEntity, Review> {
  @Override public Review transform(ReviewEntity from) {
    Review transformed = new Review();

    transformed.setId(from.getId());
    transformed.setTitle(from.getTitle());
    transformed.setRating(from.getRating());
    transformed.setAuthor(from.getAuthor());
    transformed.setMessage(from.getMessage());
    transformed.setDate(from.getDate());
    transformed.setLanguageCode(from.getLanguageCode());

    return transformed;
  }
}
