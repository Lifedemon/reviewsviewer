# Review Viewer Sample
This is the sample of Clean Architecture in Real Conditions

This project has been influenced by:

**1. Uncle Bob**

(http://blog.8thlight.com/uncle-bob/2012/08/13/the-clean-architecture.html)

**2. Fernando Cejas**

(http://fernandocejas.com/2015/07/18/architecting-android-the-evolution/)

**Technical stack:**

***Languages:*** Java and Kotlin(for unit tests)

***DI:*** Dagger

***Persistency:*** DB: ORMLite 

***Networking:*** Retrofit, Picasso, OkHttp

***Asynchronous programming:*** RxJava

***UI:*** Design Support Library, AppCompat, RecyclerView

***Cool sources:*** Retrolambda, Lombok

***Testing:*** Mockito.

**2. Next steps:**

a. Pagination

b. Functional testing: Cucumber + Espresso
